package ru.tsc.avramenko.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.tsc.avramenko.tm.model.Project;
import java.util.List;

public interface ProjectEndpointClient {

    static ProjectEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter =
                new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters =
                new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory =
                () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectEndpointClient.class, "http://localhost:8081/api/projects");
    }

    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") final String id);

    @GetMapping("/findAll")
    List<Project> findAll();

    @PostMapping("/create")
    Project create(@RequestBody final Project project);

    @PostMapping("/createAll")
    List<Project> createAll(@RequestBody final List<Project> projects);

    @PostMapping("/save")
    Project save(@RequestBody final Project project);

    @PostMapping("/saveAll")
    List<Project> saveAll(@RequestBody final List<Project> projects);

    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") final String id);

    @PostMapping("/deleteAll")
    void deleteAll();

}