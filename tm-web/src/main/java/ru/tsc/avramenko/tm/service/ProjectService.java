package ru.tsc.avramenko.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.avramenko.tm.api.service.IProjectService;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.repository.ProjectRepository;

import java.util.Collection;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @Override
    public @Nullable Project findById(@Nullable final String id) {
        return repository.findById(id).orElse(null);
    }

    @Override
    public @Nullable Project findByIdAndUserId(@Nullable final String id, @Nullable final String userId) {
        return repository.findByIdAndUserId(id, userId);
    }

    @Override
    public @Nullable Collection<Project> findAll() {
        return repository.findAll();
    }

    @Override
    public @Nullable Collection<Project> findAllByCurrentUserId(@Nullable final String userId) {
        return repository.findAllByCurrentUserId(userId);
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void clearAllByCurrentUserId(@Nullable String userId) {
        repository.removeAllByCurrentUserId(userId);
    }

    @Override
    @Transactional
    public @NotNull Project create(@Nullable Project project) {
        return repository.save(project);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeByIdAndUserId(@Nullable String id, @Nullable String userId) {
        repository.removeByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    public @NotNull Project save(@NotNull Project project) {
        return repository.save(project);
    }

}