package ru.tsc.avramenko.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.tsc.avramenko.tm.model.Result;
import ru.tsc.avramenko.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService
@RequestMapping("/auth")
public interface IAuthEndpoint {

    @PostMapping("/login")
    @WebMethod
    Result login(
            @WebParam(name = "username") @RequestParam("username") @NotNull final String username,
            @WebParam(name = "password") @RequestParam("password") @NotNull final String password
    );

    @GetMapping("/logout")
    @WebMethod
    Result logout();

    @GetMapping("/profile")
    @WebMethod
    Result profile();

}