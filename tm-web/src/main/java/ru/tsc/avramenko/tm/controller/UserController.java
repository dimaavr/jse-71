package ru.tsc.avramenko.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.service.UserService;
import ru.tsc.avramenko.tm.util.UserUtil;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/auth/user-profile")
    public ModelAndView profile() {
        final User user = userService.findById(UserUtil.getUserId());
        return new ModelAndView("user-profile", "user", user);
    }

    @PostMapping("/auth/user-profile")
    public String profile(
            @ModelAttribute("user") User user,
            BindingResult result
    ) {
        userService.updateUserById(UserUtil.getUserId(), user.getFirstName(), user.getLastName(), user.getMiddleName(), user.getEmail());
        return "redirect:/";
    }

}