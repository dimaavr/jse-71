<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>
<h1>Project List</h1>

<table width="100%" cellpadding="10" border="1" style="margin-top: 25px">
    <tr>
        <th>ID</th>
        <th>User ID</th>
        <th>Name</th>
        <th>Desc</th>
        <th>Status</th>
        <th>Start Date</th>
        <th>Finish Date</th>
        <th>Create Date</th>
        <th align="center">Edit Project</th>
        <th align="center">Delete Project</th>
    </tr>
    <c:forEach var="project" items="${projects}">
        <tr>
            <td align="center">
                <c:out value="${project.id}"/>
            </td>
            <td align="center">
                <c:out value="${project.userId}"/>
            </td>
            <td align="center">
                <c:out value="${project.name}"/>
            </td>
            <td align="center">
                <c:out value="${project.description}"/>
            </td>
            <td align="center">
                <c:out value="${project.status.getDisplayName()}"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${project.startDate}" pattern="dd.MM.yyyy - HH:mm"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${project.finishDate}" pattern="dd.MM.yyyy - HH:mm"/>
            </td>
            <td align="center">
                <fmt:formatDate value="${project.created}" pattern="dd.MM.yyyy - HH:mm"/>
            </td>
            <td align="center">
                <a style="color: green" href="/project/edit/${project.id}">Edit</a>
            </td>
            <td align="center">
                <a style="color: red" href="/project/delete/${project.id}">Delete</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px">
    <button>Create Project</button>
</form>

<jsp:include page="../include/_footer.jsp"/>