<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<style>
    h3 {
        color: black;
        text-align: center;
        padding: 12px;
        text-decoration: none;
        font-size: 18px;
        line-height: 10px;
        border-radius: 15px;
        border: 2px solid black;
        outline-color: black;
        outline-width: 3px;
        outline-style: solid;
        width: max-content;
    }
    a {
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.08);
        -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.08);
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.08);
        color: #fff;
        display:block;
        width:max-content;
        text-align: center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        padding: 6px 22px;
        margin: -69px auto;
        float: left;
        text-decoration: none;
        text-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-transition: background-color 0.1s linear;
        -moz-transition: background-color 0.1s linear;
        -o-transition: background-color 0.1s linear;
        transition: background-color 0.1s linear;
        background-color: rgb( 124, 124, 124 );
        border: 1px solid rgb( 130, 120, 110 );
    }
    table {
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        text-align: center;
        font-weight: bolder;
    }
    tr:not(.button), td:not(.button) {
        border-style: solid;
        border-color: black;
        background: rgb(232, 240, 254);
        border-radius: 6px;
    }
    input[type=submit] {
        border-radius: 4px;
        -webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.08);
        -moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.08);
        box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.08);
        color: #fff;
        display:block;
        width:max-content;
        text-align: center;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 14px;
        padding: 6px 22px;
        margin: 20px auto;
        text-decoration: none;
        text-shadow: 0 1px 1px rgba(0, 0, 0, 0.075);
        -webkit-transition: background-color 0.1s linear;
        -moz-transition: background-color 0.1s linear;
        -o-transition: background-color 0.1s linear;
        transition: background-color 0.1s linear;
        background-color: rgb( 43, 153, 91 );
        border: 1px solid rgb( 33, 126, 74 );
    }
    input[type=submit]:hover {
        background-color: rgb( 75, 183, 141 );
    }
    a:hover {
        background-color: rgb( 140, 135, 141 );
    }
</style>

<head><title>Login Page</title></head>
<body onload='document.logopass.username.focus();'>
<h3>Login in Task Manager</h3>
<form name='logopass' action='/login' method='POST'>
    <table>
        <tr>
            <td>User:</td>
            <td><input type='text' name='username' value=''></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><input type='password' name='password'/></td>
        </tr>
        <tr class="button">
            <td class="button" colspan='2'><input name="submit" type="submit" value="Login"/></td>
        </tr>
    </table>
</form>
</body>

<div class="header">
    <a href="/">Back</a>
</div>

</html>